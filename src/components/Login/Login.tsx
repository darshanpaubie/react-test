import React from 'react'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { login } from '../../redux/actions/loginAction';
import { useDispatch } from 'react-redux';
import { Dispatch } from 'redux';
import { useNavigate } from 'react-router-dom';

const validationSchema: object = yup.object({
  email: yup.string().email('Enter a valid email').required('Email is required'),
  password: yup.string().min(5, 'Password should be of minimum 5 characters length').required('Password is required'),
});

const Login: React.FC = () => {
  const dispatch: Dispatch<any> = useDispatch();
  let navigate = useNavigate()

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values: any) => {
      dispatch(login({ email: values.email, password: values.password }, navigate));
    },
  });
  return (
    <div className="App-header">
      <form onSubmit={formik.handleSubmit}>
        <TextField
          fullWidth
          type="text"
          id="email"
          name="email"
          label="Email"
          value={formik.values.email}
          onChange={formik.handleChange}
          error={formik.touched.email && Boolean(formik.errors.email)}
          helperText={formik.touched.email && formik.errors.email}
        />
        <TextField
          fullWidth
          id="password"
          name="password"
          label="Password"
          type="password"
          value={formik.values.password}
          onChange={formik.handleChange}
          error={formik.touched.password && Boolean(formik.errors.password)}
          helperText={formik.touched.password && formik.errors.password}
        />
        <Button color="primary" variant="contained" fullWidth type="submit">
          Submit
        </Button>
      </form>
    </div>
  )
}

export default Login
