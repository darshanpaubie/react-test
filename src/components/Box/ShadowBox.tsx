import React from 'react'
import PropTypes from 'prop-types';
import { Box, Card } from '@mui/material';
import { CardActionArea } from '@material-ui/core';


const ShadowBox = ({ shadow, label, count, isHover }: { shadow: string, label: string, count: number, isHover: boolean }) => {
  return (
    <Card sx={{ mb: 3, boxShadow: shadow }}>
        <CardActionArea>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    py: 4.5,
                    bgcolor: isHover?'#0d89ec':'#50b0fd',
                    color: 'grey.800'
                }}
            >
                <Box sx={{ color: 'inherit', fontSize: '15px' }}>{label}: {count}</Box>
            </Box>
        </CardActionArea>
    </Card>
  )
}
ShadowBox.propTypes = {
    shadow: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    count: PropTypes.number.isRequired,
    isHover: PropTypes.bool.isRequired,

};
export default ShadowBox