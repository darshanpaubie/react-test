import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Dispatch } from 'redux';
import { getActivityLog } from '../../redux/actions/activityAction';
import DateRangePickers from './DateRangePickers';
import Graph from './Graph'
import Statistics from './Statistics'

const Dashboard: React.FC = () => {
  const dispatch: Dispatch<any> = useDispatch();
  const [value, setValue] = React.useState<string[] | null[]>([null, null]);
  const [selectedOption, setSelectedOption] = React.useState<string>("rotations");

  useEffect(() => {
    dispatch(getActivityLog({ start_date: value[0], end_date: value[1], response_range: "daily" }))
  }, [value])

  const activityData = useSelector((state: any) => state.activity);

  return (
    <div className='dashboard'>
      <div className='dashboard-div'>
        <DateRangePickers value={value} setValue={setValue} />
      </div>
      {activityData.isLoading ? "Loading" :
        <>
          <div className='dashboard-div'>
            <Statistics data={activityData?.activityData} selectedOption={selectedOption} setSelectedOption={setSelectedOption} />
          </div>
          <div className='dashboard-div'>
            <Graph data={activityData?.activityData} selectedOption={selectedOption} />
          </div>
        </>
      }
    </div>
  )
}

export default Dashboard