import React from 'react'
import * as _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Grid } from '@mui/material';
import Chart from 'react-apexcharts';


const Graph = ({ data, selectedOption }: any) => {
    const [xAxisData, setXAxisData] = useState<string[]>(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
    const [yAxisData, setYAxisData] = useState<{ name: string, data: number[] }[]>([
        {
            name: "Automatic",
            data: [35, 125, 35, 35, 35, 80, 35, 20, 35, 45, 15, 75]
        },
        {
            name: "Manual",
            data: [10, 125, 35, 35, 35, 80, 35, 20, 35, 45, 15, 75]
        }
    ]);

    const { activityData } = useSelector((state: any) => state.activity);

    useEffect(() => {
        let xAxis: string[] = [];
        let automaticType: any = { name: "Automatic", data: [] };
        let manualType: any = { name: "Manual", data: [] }

        _.map(activityData, (raw) => { raw.time = moment(raw.start_time).format('DD-MM-YYYY') });

        const groupedByTimestamp = _.groupBy(activityData, 'time');

        _.each(groupedByTimestamp, async (logObj, time) => {
            xAxis.push(time);

            const groupedByType = _.groupBy(logObj, 'type');

            _.each(groupedByType, async (groupedByTypeData, type) => {

                if (type === "Automatic") {
                    let summationData = await _.reduce(groupedByTypeData, function (sum, n) {
                        return sum + n[selectedOption];
                    }, 0);
                    automaticType.data.push(parseFloat(summationData.toFixed(2)));
                    // @ts-ignore: Unreachable code error
                    if (groupedByType.Manual === undefined) {
                        manualType.data.push(0);
                    }
                } else if (type === "Manual") {
                    let summationData = await _.reduce(groupedByTypeData, function (sum, n) {
                        return sum + n[selectedOption];
                    }, 0);
                    manualType.data.push(parseFloat(summationData.toFixed(2)));
                    // @ts-ignore: Unreachable code error
                    if (groupedByType.Automatic === undefined) {
                        automaticType.data.push(0);
                    }
                }
            })

        })
        setXAxisData(xAxis);
        setYAxisData([automaticType, manualType]);
    }, [data, selectedOption]);

    return (
        <div>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Chart height={480} type={'bar'} options={{ chart: { id: "bar-chart", stacked: true, toolbar: { show: true }, zoom: { enabled: true } }, responsive: [{ breakpoint: 480, options: { legend: { position: "bottom", offsetX: -10, offsetY: 0 } } }], plotOptions: { bar: { horizontal: false, columnWidth: '50%' } }, xaxis: { type: "category", categories: xAxisData }, legend: { show: true, fontSize: "14px", fontFamily: `"Roboto",sans-serif`, position: "bottom", offsetX: 20, labels: { useSeriesColors: false }, markers: { width: 16, height: 16, radius: 5 }, itemMargin: { horizontal: 15, vertical: 8 } }, fill: { type: "solid" }, dataLabels: { enabled: false }, grid: { show: true } }} series={yAxisData} />
                </Grid>
            </Grid>

        </div>
    );
}

Graph.propTypes = {
    data: PropTypes.array,
    selectedOption: PropTypes.string,
};

export default Graph