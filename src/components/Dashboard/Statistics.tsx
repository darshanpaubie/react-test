import React, { useEffect, useState } from 'react'
import { Grid } from '@mui/material';
import ShadowBox from '../Box/ShadowBox';

const Statistics = ({ data, selectedOption, setSelectedOption }: any) => {
    const [stride, setStride] = useState<number>(0);
    const [calories, setCalories] = useState<number>(0);
    const [miles, setMiles] = useState<number>(0);
    const [time, setTime] = useState<number>(0);

    useEffect(() => {
        let strideCount: number = 0;
        let caloriesCount: number = 0;
        let milesCount: number = 0;
        let timeCount: number = 0;
        data?.forEach((element: any) => {
            if (element.calories) {
                caloriesCount += element.calories;
                milesCount += element.distance;
                timeCount += element.duration;
                strideCount += element.rotations;
            }
        });
        setStride(parseFloat(strideCount.toFixed(2)));
        setCalories(parseFloat(caloriesCount.toFixed(2)));
        setMiles(parseFloat(milesCount.toFixed(2)));
        setTime(parseFloat(timeCount.toFixed(2)));
    }, [data])


    return (
        <Grid container spacing={1}>
            <Grid item xs={2} sm={2} md={2} lg={2} >
                <div onClick={() => setSelectedOption("rotations")}>
                    <ShadowBox shadow={((selectedOption === "rotations") ? "3" : "0")} label="Strides" count={stride} isHover={((selectedOption === "rotations") ? true : false)} />
                </div>
            </Grid>
            <Grid item xs={2} sm={2} md={2} lg={2}>
                <div onClick={() => setSelectedOption("calories")}>
                    <ShadowBox shadow={((selectedOption === "calories") ? "3" : "0")} label="Calories" count={calories} isHover={((selectedOption === "calories") ? true : false)} />
                </div>
            </Grid>
            <Grid item xs={2} sm={2} md={2} lg={2}>
                <div onClick={() => setSelectedOption("distance")}>
                    <ShadowBox shadow={((selectedOption === "distance") ? "3" : "0")} label="Miles" count={miles} isHover={((selectedOption === "distance") ? true : false)} />
                </div>
            </Grid>
            <Grid item xs={2} sm={2} md={2} lg={2}>
                <div onClick={() => setSelectedOption("duration")}>
                    <ShadowBox shadow={((selectedOption === "duration") ? "3" : "0")} label="Time" count={time} isHover={((selectedOption === "duration") ? true : false)} />
                </div>
            </Grid>
        </Grid>
    )
}

export default Statistics