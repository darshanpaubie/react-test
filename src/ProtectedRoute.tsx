import { Navigate, Outlet } from 'react-router-dom';

export default function ProtectedRoute({ children, ...rest }: any) {
    const auth = !!localStorage.getItem("token");
    if (auth) {
        return <Outlet />;
    } else {
        return <Navigate to={'login'} replace={true} />;
    }
}