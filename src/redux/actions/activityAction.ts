import axios from 'axios';
import { environment } from '../../environments/environment';

export const getActivityLog = (params: any) => {
    return async (dispatch: (arg0: { type: string; payload?: any }) => void) => {
        try {
            dispatch({ type: 'GET_ACTIVITY_LOG_REQUEST' })
            console.log(params)
            if (params.start_date === null || params.end_date === null) {
                dispatch({ type: 'GET_ACTIVITY_LOG_FAILURE', payload: null })
            } else {
                let startDate = `${params.start_date?.$y}-${params.start_date?.$M + 1}-${params.start_date?.$D}`;
                let endDate = `${params.end_date?.$y}-${params.end_date?.$M + 1}-${params.end_date?.$D}`;
                
                const loginResp = await axios({
                    method: 'get',
                    url: `${environment.apiBaseUrl}/users/74471/progress/activity_log/?start_date=${startDate}&end_date=${endDate}&response_range=daily`,
                    data: {},
                    headers: { 'Authorization': `Token ${localStorage.getItem("token")}` }
                });
                if (loginResp.status === 200) {
                    dispatch({ type: 'GET_ACTIVITY_LOG_SUCCESS', payload: loginResp.data.data });
                } else {
                    dispatch({ type: 'GET_ACTIVITY_LOG_FAILURE', payload: loginResp })
                }
            }
        } catch (error) {
            console.log('error: ', error)
        }
    }
}

