import axios from 'axios';
import { NavigateFunction } from 'react-router-dom';
import { environment } from '../../environments/environment';

export const login = (params: any, navigate: NavigateFunction) => {
    return async (dispatch: (arg0: { type: string; payload?: any }) => void) => {
        try {
            console.log(params)
            dispatch({ type: 'LOGIN_REQUEST' })
            const loginResp = await axios({
                method: 'post',
                url: `${environment.apiBaseUrl}/login/`,
                data: {
                    email: params.email,
                    password: params.password
                },
            });
            if (loginResp.status === 200) {
                dispatch({ type: 'LOGIN_SUCCESS', payload: loginResp.data.data });
                navigate('/dashboard');
            } else {
                dispatch({ type: 'LOGIN_FAILURE', payload: loginResp })
            }
        } catch (error) {
            console.log('error: ', error)
        }
    }
}

