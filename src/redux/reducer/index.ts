import { combineReducers } from 'redux';
import AuthReducer from './authReducer';
import ActivityReducer from './activityReducer';

export default combineReducers({
    authReducer: AuthReducer,
    activity: ActivityReducer
})
