const initialState = {
    isLoading: false,
    errorMessage: null,
    activityData: null,
  }
  type ACTIONABLE = { type: string; payload: any };
  
  const activityReducer = (state = initialState, { type, payload }: ACTIONABLE) => {
    switch (type) {
      case 'GET_ACTIVITY_LOG_REQUEST':
        return {
          ...state,
          isLoginLoading: true,
        }
  
      case 'GET_ACTIVITY_LOG_SUCCESS':
        return {
          ...state,
          isLoginLoading: false,
          activityData: payload,
        }
  
      case 'GET_ACTIVITY_LOG_FAILURE':
        return {
          ...state,
          isLoginLoading: false,
          errorMessage: payload,
        }
      default:
        return state
    }
  }
  
  export default activityReducer
  