const initialState = {
  isLoginLoading: false,
  errorMessage: null,
  loginData: null,
}
type ACTIONABLE = { type: string; payload: any };

const authReducer = (state = initialState, { type, payload }: ACTIONABLE) => {
  switch (type) {
    case 'LOGIN_REQUEST':
      return {
        ...state,
        isLoginLoading: true,
      }

    case 'LOGIN_SUCCESS':
      localStorage.setItem("token", payload?.token);
      return {
        ...state,
        isLoginLoading: false,
        loginData: payload,
      }

    case 'LOGIN_FAILURE':
      return {
        ...state,
        isLoginLoading: false,
        errorMessage: payload,
      }
    default:
      return state
  }
}

export default authReducer
